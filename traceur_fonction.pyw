# -*- coding: utf-8 -*-

from pylab import *
from tkinter import *
from math import*


def tracer():

    # récupération des valeurs
    equation = v.get().replace('x','{}') # fonction entrée
    epaisseur=largeur.get() # épaisseur de ligne réglée avec le curseur
    if xmin.get()=="" : x1=-5.0
    else : x1=float(eval(xmin.get()))
    if xmax.get()=="" : x2=5.0
    else : x2=float(eval(xmax.get()))
    if ymin.get()=="" : y1=-5.0
    else : y1=float(eval(ymin.get()))
    if ymax.get()=="" : y2=5.0
    else : y2=float(eval(ymax.get()))
    
    x = linspace(x1,x2) # création du tableau des valeurs des abscisses entre xmin et xmax

    y=[]
    for e in x :
        y.append(eval(equation.format(e)))  # évaluation de la fonction et affectation au tableau des y

    plot(x,y,linewidth=epaisseur) # tracé de la courbe
 
    xlim(x1,x2) # limites des axes
    ylim(y1,y2)

    # axes
    ax = gca()
    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    ax.xaxis.set_ticks_position('bottom')
    ax.spines['bottom'].set_position(('data',0))
    ax.yaxis.set_ticks_position('left')
    ax.spines['left'].set_position(('data',0))

    show() # affichage de la courbe

def callback(event) :
        tracer()

# PROGRAMME PRINCIPAL

# fenêtre principale    
root = Tk()

root.title('Traceur de courbes')
root.configure(width=True,height=False)
root.grid_columnconfigure(0,weight=1)
root.grid_columnconfigure(1,weight=1)
root.grid_columnconfigure(2,weight=1)
root.grid_columnconfigure(3,weight=1)
root.grid_columnconfigure(4,weight=1)


# étiquettes + champ entrée expression des fonctions
lb1 = Label(root,text='Fonction :')
lb1.grid(column=0,row=0,sticky='e')

v = StringVar()
fonction1 = Entry( root , textvariable = v )
fonction1.grid(column=1,row=0,sticky='w',columnspan = 4)

# largeur de ligne
largeur = Scale(root, orient='horizontal', from_=1, to=5,resolution=1, tickinterval=1, length = 200 , label='Largeur de ligne')
largeur.grid(column=0, row = 8, columnspan = 4)

# limites des axes
lb6 = Label( root , text = 'xmin')
lb6.grid(column = 0 , row = 10)
lb7 = Label( root , text = 'xmax')
lb7.grid(column = 1 , row = 10)
lb8= Label( root , text = 'ymin')
lb8.grid(column = 2 , row = 10)
lb9 = Label( root , text = 'ymax')
lb9.grid(column = 3 , row = 10)

xmin=StringVar()
xmin = Entry( root , textvariable = xmin)
xmin.grid(column = 0 , row = 11)
xmax=StringVar()
xmax = Entry( root , textvariable = xmax)
xmax.grid(column = 1 , row = 11 )
ymin=StringVar()
ymin = Entry( root , textvariable = ymin)
ymin.grid(column = 2 , row = 11)
ymax=StringVar()
ymax = Entry( root , textvariable = xmax)
ymax.grid(column = 3 , row = 11)

# appuyer depuis nimporte où sur <ENTREE> pour lancer le tracer
root.bind("<Return>",callback)
root.bind("<KP_Enter>",callback)

# bouton tracer
bouton_tracer = Button(root, text='Tracer', command = tracer)
bouton_tracer.grid(column=0,row=16,columnspan=4)

root.mainloop()
